--[[
    PART 1: DATA TYPES, VARIABLE, AND SCOPES
    There are eight basic types in Lua.
    The "beginner" types are:
        1. nil
        2. number
        3. string
        4. boolean
    The "intermediate" types:
        5. table
        6. function
    The "advanced" type:
        7. thread
    The "exotic" type:
        8. userdata (Basically a void* from the C API)
    Note: variables don't have an associated type in Lua. Data has an
    associated type, and a variable can store data of any type.
    Just like in any dynamically typed language.
]]--

-- The basics:
--   nil, number, string, and boolean.
print(type(nil))  -- Setting a variable to nil "unsets" it.
print(type(1)) -- All numbers are stored as 64 bit doubles under the hood.
print(type(3.14)) -- Still a 64 bit double under the hood.
print(type('hello')) -- Immutable strings just like in Python and Javascript.
print (type("hello")) -- Use single or double quote, it don't matter.
print(type([[
    this
    is
    a
    multi-line
    string
]]))
print(type(true))
print(type(false))

-- The intermediate:
--   table, and function.
-- Example of a table as an array. Indexing starts from 1. :O
print(type({1, 2, 'hello'}))
-- Example of a table as a dictionary (associative array):
local example_table = {key = 'value', ['@'] = 'another value'}
print(type(example_table))
-- Functions are first class objects in Lua.
print(type(function () print('hello') end))

-- The advanced and exotic type can be dealt with later.

-- Any undefined variable has a value of nil by default.
-- This is why setting a variable to nil will unset it.
-- By default variable will be globally scoped.
-- using the 'local' keyword restricts a variable to it's scope (like what 'let' does in Javascript).
-- Use do/end to open a new scope.
-- Function, Loop, and Conditional bodies are have scopes of their own.
print('scope and variables:')
do
    print(V)  -- V has not been defined yet but with this reference, it will become defined (globally) and set to nil.
    V = true
    print(V)
    V = nil  -- unsetting.
    print(V)
    V = true

    local a = false
    print(a)
end
print(V)  -- Because V is now global.
---@diagnostic disable-next-line: undefined-global
print(a)  -- a is now redefined as a global variable since it the previous declaration was scoped. Value: nil.
print('end scope and variables')

-- Only nil and false are falsy; 0 and '' are true!
if 0 then print('0 is true') else print('0 is false') end
if '' then print("'' is true") else print("'' is false") end
if nil then print('wot?') else print('expected')end
if false then print('wot??') else print('expected') end