--[[
    PART THREE: FUNCTIONS
]]--

-- Lua's functions are first class objects just like in Python and Javascript
-- but unlike in C.
-- Lua supports closures! Combined with first class functions, this is VERY
-- powerful.
print("Functions as first class objects; support for closures:")
local function get_foo()
    local x = 0 
    local g = function ()
        local y = 0
        -- Example of anonymous functions. Technically all of them are
        -- anonymous and then just bound to the var they are assigned to.
        -- This var can be unset, effectively "deleting" the function.
        return function ()
            print(string.format("%d %d", x, y))
            x = x + 1
            y = y + 1
        end
    end
    return g()
end

local g = get_foo()
for k = 0, 3, 1 do
    g()
end
print("\n")


-- Lua supports (proper/optimized) tail call recursion! I'm in heaven now :D
-- To quote the docs, "Because a proper tail call uses no stack space, there is
-- no limit on the number of "nested" tail calls that a program can make.
print("Using proper tail call recursion:")
local function factorial(n)
    local function _factorial(m, acc)
        print(string.format("Debug: %d %d", m, acc))
        if m < 2 then
            return acc
        end
        return _factorial(m - 1, m * acc)
    end
    return _factorial(n, 1)
end

print(string.format("5! = %d", factorial(5)))
print("\n")


-- If an argument is not provided, by default it is assumed to be nil.
print("The default argument value is always nil.")
local f = function (arg)
    print(arg)
end
f()
print("\n")


-- Lua functions support multiple return (a fresh breath of air compared to C,
-- nothing new compared to Python or Javascript). These variables can be
-- "unpacked" on return, but not in the same way as in Python (1-1 mapping in
-- Lua vs 1-rest in Python).
print("Demonstrating multiple return:")
f = function ()
    return 1, 2, 3
end
local w, x, y, z
x, y = f()  -- 3 is discarded. In Python, y = (2, 3).
print(x, y)
w, x, y, z = f()  -- z is nil.
print(w, x, y, z)
print("\n")