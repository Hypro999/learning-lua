--[[
    PART TWO: CONTROL FLOW (CONDITIONALS, LOOPS) AND SCOPE
        - if/elseif/else
        - ternary operator
        - 3 types of loops (same as in C)
        - For for loops, the end is inclusive and the variable is always
          locally scoped.
]]--

-- Simple if construct.
local v = 1
if v > 0 then
    print('Yes.')
else
    print('No.')
end

v = -1
if v > 0 then
    print('Yes.')
else
    print('No.')
end

v = 0
if v > 0 then
    print('Yes.')
elseif v == 0 then  -- Use ~= for what != does in Python.
    print('Yes/No.')
else
    print('No.')
end

-- Ternary operator using and/or (is shortcircuited)
local f = function ()
    print("You shouldn't see this")
    return 'no'
end
v = 1 == 1 and 'yes' or f()

-- While loops:
local i = 1  -- in honour of 1-indexing in Lua.
print('While:')
while i < 4 do
    print(i)
    i = i + 1  -- The only way to write this. No ++ or +=.
end

-- For loops:
print('For:')
for j = 1, 3, 1 do -- start, end (inclusive unlike in most languages), step value.
    print(j)
end
-- The loop variable is always locally defined. So now j is undefined (using it would yield nil and a new global variable j).

-- Repeat until (do while) loops:
i = 3
print('Repeat:')
repeat
    print(i)
    i = i - 1
until i == 0