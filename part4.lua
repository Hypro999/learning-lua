--[[
    PART FOUR: TABLES (BASICS)
    Lua's one and only composite data structure.
    It can work as an array, a dictionary, a set, whatever!
    It's almost every single data structure represented with a single mechanism.

    Quite similar to PHP's associative array. But let's not talk about it that
    way ever again please. They also support prototypal inheritance just like
    Javascript objects.

    _G is a special table of all globals.
]]--

-- As arrays (1-indexed):
local a;
a = {10, 'hello', true}
for i, val in pairs(a) do
    print(i, val)
end
print('\n')
print(a[1], a[2], a[3])

-- As dictionaries:
a = {['key1'] = 'value1', key2 = 'value2', [3.14] = 'pi'}
for i, val in pairs(a) do
    print(i, val)
end
print('\n')
print(a.key1, a['key2'], a[3.14]) -- Go wild.
print(a.something) -- Just like an unset variable, the key is created and = nil.
print('\n')

-- Things get more complicated if we were to use an object for a key.
-- This is because with objects, the addresses are compared.
a = {[{}] = 'boo'}
print(a[{}])
print('\n')


-- As both an array and a dictionary. Go even more wild?
a = {'val1', key='val', 'val2'}
for i, val in pairs(a) do
    print(i, val)
end
print('\n')

a = {'val1', key='val', 'val2', [1] = 'val3'}  -- Doesn't overwrite
for i, val in pairs(a) do
    print(i, val)
end
print('\n')

a = {[2] = 'VAL2', 'val1', key='val', 'val2', [1] = 'val3'}  -- Keys have lower precedence
for i, val in pairs(a) do
    print(i, val)
end
print('\n')

a = {[3] = 'hewwo'}  -- Works
for i, val in pairs(a) do
    print(i, val)
end
print('\n')