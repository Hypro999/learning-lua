--[[
    PART FIVE: TABLES (INTERMEDIATE)
        - Metatables and metamethods.
        - Getting and setting metatables can only be done via.
          getmetatable and setmetatable.
]]--

math = require('math')

-- Demonstration of the metatable and __index.

local cart_metatable = {
    __index = {
        price = function (self)
            local p = 0
            for _, item in pairs(self.items) do
                p = p + item.quantity * item.unit_price
            end
            return p
        end
    }
}

local cart1 = {
    items = {
        {
            name = 'carrots',
            quantity = 10,
            unit_price = .75
        },
        {
            name = 'apples',
            quantity = 5,
            unit_price = 1.2
        }
    }
}
setmetatable(cart1, cart_metatable)
-- Using cart1:price() causes cart1 to be passed as the first (self) parameter.
print(string.format("A regular cart: %d", cart1:price()))

local cart2 = {
    items = {
        {
            name = 'carrots',
            quantity = 11,
            unit_price = .75
        },
    },
    price = function (self)
        local p = 0
        for _, item in pairs(self.items) do
            p = p + math.ceil(item.quantity / 2) * item.unit_price  -- BOGOF
        end
        return p
    end
}
setmetatable(cart2, cart_metatable)
-- Proof that the metatable __index only provides defaults for dot loopup (or
-- equivalently colon lookup) which can be overwritten.
print(string.format("A bogof cart: %d", cart2:price()))

-- Some extra notes from learnxinyminutes:
-- Direct table lookups that fail will retry using the metatable's __index
-- value, and this recurses.
-- An __index value can also be a function(tbl, key) for more customized
-- lookups.

-- Overridable meta methods (operators):
-- __add(a, b)                     for a + b
-- __sub(a, b)                     for a - b
-- __mul(a, b)                     for a * b
-- __div(a, b)                     for a / b
-- __mod(a, b)                     for a % b
-- __pow(a, b)                     for a ^ b
-- __unm(a)                        for -a
-- __concat(a, b)                  for a .. b
-- __len(a)                        for #a
-- __eq(a, b)                      for a == b
-- __lt(a, b)                      for a < b
-- __le(a, b)                      for a <= b
-- __index(a, b)  <fn or a table>  for a.b
-- __newindex(a, b, c)             for a.b = c
-- __call(a, ...)                  for a(...)